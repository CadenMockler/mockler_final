﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTime : MonoBehaviour
{

    public TimeManager timeManager;
    public GameObject slowAbility;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            timeManager.DoSlowmotion();
        }

        slowAbility.SetActive(false);
    }
}
