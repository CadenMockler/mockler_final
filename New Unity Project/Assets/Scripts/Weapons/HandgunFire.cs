﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandgunFire : MonoBehaviour
{

    public GameObject theGun;
    public GameObject muzzleFlash;
    public AudioSource gunFire;
    public AudioSource emptySound;
    public bool isFiring = false;
    public float targetDistance;
    public int damageAmount = 5;

    void Update() //Check if pressing fire button
    {
        if (Input.GetButtonDown("Fire1")) 
        {
            if (GlobalAmmo.handgunAmmo < 1)
            {
                emptySound.Play();
            }

            else 
            {
                if (isFiring == false) //Not firing start coroutine
                {
                    StartCoroutine(FiringHandgun());
                }
            }
        }
    }

    IEnumerator FiringHandgun() //Firing coroutine
    {
        RaycastHit theShot;
        isFiring = true;
        GlobalAmmo.handgunAmmo -= 1;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out theShot))
        {
            targetDistance = theShot.distance;
            theShot.transform.SendMessage("DamageEnemy", damageAmount, SendMessageOptions.DontRequireReceiver);
        }

        theGun.GetComponent<Animator>().Play("HandGunFire"); //Play animation
        muzzleFlash.SetActive(true);
        gunFire.Play(); //Plays audio
        yield return new WaitForSeconds(0.05F); //Is firing for 0.05 seconds
        muzzleFlash.SetActive(false);
        yield return new WaitForSeconds(0.025f); //Can play again for another 0.025 seconds
        theGun.GetComponent<Animator>().Play("New State");
        isFiring = false;


    }
}
