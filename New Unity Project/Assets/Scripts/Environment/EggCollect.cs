﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EggCollect : MonoBehaviour
{
    public GameObject eggClutch;
    public AudioSource collectSound;

    void OnTriggerEnter(Collider other)
    {
        GlobalScore.scoreValue += 500;
        GlobalComplete.treasureCount += 1;
        eggClutch.SetActive(true);
        collectSound.Play();
        GetComponent<BoxCollider>().enabled = false;
        this.gameObject.SetActive(false);
    }
}
